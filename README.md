# Software Studio 2018 Spring Midterm Project
## Notice
* Replace all [xxxx] to your answer

## Topic
* [Topic Name]
* Key functions (add/delete)
    1. user page
    2. post page
    3. post list page
    4. [xxx]
* Other functions (add/delete)
    1. [xxx]
    2. [xxx]
    3. [xxx]
    4. [xxx]

## Basic Components
|Component|Score|Y/N|
|:-:|:-:|:-:|
|Membership Mechanism|20%|Y|
|GitLab Page|5%|Y|
|Database|15%|Y|
|RWD|15%|Y|
|Topic Key Function|15%|Y|

## Advanced Components
|Component|Score|Y/N|
|:-:|:-:|:-:|
|Third-Party Sign In|2.5%|Y|
|Chrome Notification|5%|N|
|Use CSS Animation|2.5%|Y|
|Security Report|5%|N|
|Other functions|1~10%|N|

## Website Detail Description
FORUM
可以用email和google登入
登入之後可以在post頁面可以發文，在account可以看到自己發過的文
在account頁面有css動畫
有RWD
https://105000004.gitlab.io/Midterm_Project/
## Security Report (Optional)
