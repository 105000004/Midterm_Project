function init() {
    var user_email = '';
    firebase.auth().onAuthStateChanged(function (user) {
        var menu = document.getElementById('bartext');
        //Check user login
        if (user) {
            user_email = user.email;
            alert(user_email);
            document.getElementById('emailaccount').innerHTML = user_email;
        }

    });
    
    var str_before_username = "<div class='my-3 p-3 bg-white rounded box-shadow'><p class='media-body pb-3 mb-0 small lh-125 border-bottom border-gray'><strong class='d-block text-gray-dark'>";

    var str_after_content = "</p></div></div>\n";

    var postsRef = firebase.database().ref('com_list');
    // List for store posts html
    var total_post = [];
    // Counter for checking history post update complete
    var first_count = 0;
    // Counter for checking when to update new post
    var second_count = 0;
    //element.innerHTML=user_email;
    
    postsRef.once('value')
        .then(function (snapshot) {
            
            snapshot.forEach(function(childSnapshot){
                first_count++;
                if(childSnapshot.val().email == user_email)
                    total_post.push(    '</strong>' + childSnapshot.val().data + str_after_content);
            })
            document.getElementById('post_list').innerHTML = total_post.join('');
            postsRef.on('child_added', function(childSnapshot) {
                second_count++;
                if(second_count > first_count){
                    if(childSnapshot.val().email == emailaccount){
                        total_post.push( '</strong>' + childSnapshot.val().data + str_after_content);
                        document.getElementById('post_list').innerHTML = total_post.join('');
                    }
                }
            });
        })
        .catch(e => console.log(e.message));
}

window.onload = function () {
    init();
    
};

