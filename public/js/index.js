
function init() {
    var user_email = '';
    firebase.auth().onAuthStateChanged(function (user) {
        var menu = document.getElementById('bartext');
        //Check user login
        if (user) {
            user_email = user.email;
            menu.innerHTML = "<span class='nav-item'>" + "</span><span class='nav-item' id='logout-btn'>Logout</span>";
            document.getElementById('tousershow').innerHTML = user_email;
            /// TODO 5: Complete logout button event
            ///         1. Add a listener to logout button 
            ///         2. Show alert when logout success or error (use "then & catch" syntex)
            var btnLogout = document.getElementById('logout-btn');
            btnLogout.addEventListener('click', function(){
                firebase.auth().signOut().then(function(){
                    alert('Logout!');
                    user = 0;
                }).catch(e => alert(e.message));
                window.location = "post.html";
            });
        } else {
            // It won't show any post if not login
            menu.innerHTML = "<a class='dropdown-item' href='signin.html'>Login</a>";
            document.getElementById('post_list').innerHTML = "";
        }

    });
    to_post_page = document.getElementById('to_post');
    to_post_page.addEventListener('click', function () {
        window.location = "post.html";
    });

    //post code used to be

    // The html code for post
    var str_before_username = "<div class='my-3 p-3 bg-white rounded box-shadow'><p class='media-body pb-3 mb-0 small lh-125 border-bottom border-gray'><strong class='d-block text-gray-dark'>\n";

    var str_after_content = "</p></div></div>\n";

    var postsRef = firebase.database().ref('com_list');
    // List for store posts html
    var total_post = [];
    // Counter for checking history post update complete
    var first_count = 0;
    // Counter for checking when to update new post
    var second_count = 0;

    postsRef.once('value')
        .then(function (snapshot) {
            /// TODO 7: Get all history posts when the web page is loaded and add listener to update new post
            ///         1. Get all history post and push to a list (str_before_username + email + </strong> + data + str_after_content)
            ///         2. Join all post in list to html in once
            ///         4. Add listener for update the new post
            ///         5. Push new post's html to a list
            ///         6. Re-join all post in list to html when update
            ///
            ///         Hint: When history post count is less then new post count, update the new and refresh html
            
            snapshot.forEach(function(childSnapshot){
                first_count++;
                total_post.push(  str_before_username+childSnapshot.val().email + '</strong>' + childSnapshot.val().data + str_after_content);
            })
            document.getElementById('post_list').innerHTML = total_post.join('');
            postsRef.on('child_added', function(childSnapshot) {
                second_count++;
                if(second_count > first_count){
                    total_post.push(str_before_username+childSnapshot.val().email + '</strong>' + childSnapshot.val().data + str_after_content);
                    document.getElementById('post_list').innerHTML = total_post.join('');
                }
            });

        })
        .catch(e => console.log(e.message));
}

window.onload = function () {
    init();
};
